FROM ruby:2.4.1

RUN curl -sL https://deb.nodesource.com/setup_8.x | bash -

RUN apt-get update && apt-get install -qq -y build-essential nodejs mysql-client postgresql-client sqlite3 libpq-dev --fix-missing --no-install-recommends && rm -rf /var/lib/apt/lists/*

RUN npm install --global yarn

ENV INSTALL_PATH /app

ENV RAILS_ENV production

RUN mkdir -p $INSTALL_PATH

WORKDIR $INSTALL_PATH

ADD Gemfile $INSTALL_PATH/Gemfile

ADD Gemfile.lock $INSTALL_PATH/Gemfile.lock

ADD .gemrc $INSTALL_PATH/.gemrc

RUN bundle install --full-index