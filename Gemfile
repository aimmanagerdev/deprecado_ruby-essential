source 'https://rubygems.org'

git_source(:github) do |repo_name|
  repo_name = "#{repo_name}/#{repo_name}" unless repo_name.include?('/')
  "https://github.com/#{repo_name}.git"
end

ruby '2.4.1'

gem 'rails', '~> 5.1', '>= 5.1.4'

gem 'activerecord-postgis-adapter', '~> 5.0', '>= 5.0.2'
gem 'bcrypt', '~> 3.1', '>= 3.1.11'
gem 'faraday', '~> 0.12.1'
gem 'health_check', '~> 2.6'
gem 'nokogiri', '~> 1.8', '>= 1.8.1'
gem 'pg', '~> 0.21.0'
gem 'puma', '~> 3.10'
gem 'oj', '~> 3.3', '>= 3.3.8'

gem 'dry-auto_inject', '~> 0.4.4'
gem 'rx', '~> 0.0.3'
gem 'rint', '~> 0.2.0'

# Use Redis adapter to run Action Cable in production
gem 'redis', '~> 3.3', '>= 3.3.3'
gem 'redis-namespace', '~> 1.5', '>= 1.5.3'
gem 'redis-rails', '~> 5.0', '>= 5.0.2'

gem 'sidekiq', '~> 5.0', '>= 5.0.4'
gem 'sidekiq-cron', '~> 0.6.3'
gem 'sidekiq-status', '~> 0.6.0'

group :development, :test do
  gem 'rspec-rails', '~> 3.6'

  gem 'database_cleaner', '~> 1.6', '>= 1.6.1'
  gem 'factory_girl_rails', '~> 4.8'
  gem 'faker', '~> 1.7', '>= 1.7.3'
  gem 'shoulda-matchers', '~> 3.1', '>= 3.1.1'

  # Call 'byebug' anywhere in the code to stop execution and get a debugger console
  gem 'byebug', platforms: %i[mri mingw x64_mingw]
end

group :development do
  gem 'annotate', '~> 2.7', '>= 2.7.2'
  gem 'awesome_print', '~> 1.8'
  gem 'dotenv-rails', '~> 2.2', '>= 2.2.1'
  gem 'letter_opener', '~> 1.4', '>= 1.4.1'

  gem 'bullet', '~> 5.5', '>= 5.5.1'
  gem 'listen', '>= 3.0.5', '< 3.2'
  # Spring speeds up development by keeping your application running in the background. Read more: https://github.com/rails/spring
  gem 'spring'
  gem 'spring-watcher-listen', '~> 2.0.0'

  # Access an IRB console on exception pages or by using <%= console %> anywhere in the code.
  gem 'web-console', '>= 3.3.0'
end

# Windows does not include zoneinfo files, so bundle the tzinfo-data gem
gem 'tzinfo-data', platforms: %i[mingw mswin x64_mingw jruby]
